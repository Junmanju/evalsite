import React from 'react'

import { Table, Row, Col } from 'antd';//

export default function CommonTable(props) {
  
    return (
        <div>
            <Table loading={props.loadingProps} rowKey={props.rowKey} columns={props.columns} dataSource={props.data}  expandable={props.expandableProps()} size="middle" />
        </div>
    )
}
