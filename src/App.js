import React, {useState, useEffect} from 'react';
import './App.css';
import CommonTable from './Components/CommonTable';
import axios from 'axios';
import { Row, Col, Layout, Tag, message } from 'antd';//

function App() {
  const [races, setRaces] = useState();
  const [raceEvent, setYear] = useState([]);
  const [loading, setLoading] = useState(false);
  
  useEffect (()=>{
    const raceResults = async function(){
        try {
            setLoading(true)
            const {data} = await axios({ 
                method: 'get',
                url: 'https://ergast.com/api/f1/2004/1/results.json',
            })
            message.success('Fetched data from sample API');
            displayTable(data);
            displayHeader(data);
        } catch (error) {
            message.error('Failed to fetch free API');
            message.info('Fetching data from database');
            axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            const {data} = await axios({ 
                method: 'get',
                url: 'https://evaluationserver.herokuapp.com/race-result',
            })
            message.success('Fetched data from backup API');   
            displayTable(data[0]);
            displayHeader(data[0]);
        }
        setLoading(false)
    }
    raceResults();   
  },[])

  const { Header, Content } = Layout;

  const displayTable = function(data){
    let results = data.MRData.RaceTable.Races[0].Results;
        results.map((result)=>{
            result.name = result.Driver.givenName+' '+result.Driver.familyName
        }) 
 
    setRaces(results);
  }
  const displayHeader = function(data){
      let showHeader = data.MRData;
 
      setYear(showHeader);
  }

  const columns = [
      {
        title: 'Finisher',
        dataIndex: 'position',
        key: 'position',
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        sorter: {
          compare: (a, b) => a.name.localeCompare(b.name),
        },
      },
      {
        title: 'Number',
        dataIndex: 'number',
        key: 'number'
      },
      {
        title: 'Laps',
        dataIndex: 'laps',
        key: 'laps',
        sorter: {
          compare: (a, b) => a.laps - b.laps,
        },
      },
      {
        title: 'Points',
        dataIndex: 'points',
        key: 'points'
      }
    ]

    const expandable = () => ( 
     {
        expandedRowRender: record => 
            <Row>
                <Col span={6}><h4>Driver Info :</h4>
                    <p><i>Code</i> : {record.Driver.code}</p>
                    <p><i>Name</i> : {record.Driver.givenName+' '+record.Driver.familyName}</p>
                    <p><i>Birthday</i> : {record.Driver.dateOfBirth}</p>
                    <p><i>Nationality</i> : {record.Driver.nationality}</p>
                    <p><i>More Info</i> : <a href={record.Driver.url}>{record.Driver.url}</a></p>
                </Col>
                <Col span={6}>
                    <h4>Fastest Lap :</h4>
                    <p><i>Average Speed</i> : {record.FastestLap.AverageSpeed.speed} {record.FastestLap.AverageSpeed.units}</p>
                    <p><i>Time</i> : {record.FastestLap.Time.time}</p>
                    <p><i>Lap</i> : {record.FastestLap.lap}</p>
                    <p><i>Rank</i> :   <Tag color="processing">{record.FastestLap.rank}</Tag></p>
                  
                </Col>
                <Col span={12}><h4>Constructor :</h4>
                    <p>{record.Constructor.name}</p>
                    <p>{record.Constructor.nationality}</p>
                    <a href={record.Constructor.url}>{record.Constructor.url}</a>
                </Col>
            </Row>,
        rowExpandable: record => record.name !== 'Not Expandable',
  })

  const renderHeader = function(){

      if (raceEvent.length != 0){
          return <Row>
          <Col span={24}>
            <p>{raceEvent.RaceTable.Races[0].raceName}</p>
            <p>{raceEvent.RaceTable.season}</p>
            <a href={raceEvent.RaceTable.Races[0].url}>{raceEvent.RaceTable.Races[0].url}</a>
          </Col>
        </Row>
      }
  }

  return (
    <div className="App">
        <div className="container">
            <Layout>
                <Header class="header">
                    {renderHeader()}
                </Header>
                <Content>
                    <CommonTable loadingProps={loading} rowKey ='position' data={races} columns={columns} expandableProps={expandable}></CommonTable>
                </Content>
            </Layout>
        </div>             
    </div>
  );
}

export default App;
